import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Template1LanderComponent } from './templates/template-1/template-1-lander/template-1-lander.component';
import { Template1CheckoutComponent } from './templates/template-1/template-1-checkout/template-1-checkout.component';
import { Template1ThankyouComponent } from './templates/template-1/template-1-thankyou/template-1-thankyou.component';
import { Template2LanderComponent } from './templates/template-2/template-2-lander/template-2-lander.component';
import { Template2CheckoutComponent } from './templates/template-2/template-2-checkout/template-2-checkout.component';
import { Template2ThankyouComponent } from './templates/template-2/template-2-thankyou/template-2-thankyou.component';

@NgModule({
  declarations: [
    AppComponent,
    Template1LanderComponent,
    Template1CheckoutComponent,
    Template1ThankyouComponent,
    Template2LanderComponent,
    Template2CheckoutComponent,
    Template2ThankyouComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
