import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template1CheckoutComponent } from './template-1-checkout.component';

describe('Template1CheckoutComponent', () => {
  let component: Template1CheckoutComponent;
  let fixture: ComponentFixture<Template1CheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template1CheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template1CheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
