import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template1LanderComponent } from './template-1-lander.component';

describe('Template1LanderComponent', () => {
  let component: Template1LanderComponent;
  let fixture: ComponentFixture<Template1LanderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template1LanderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template1LanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
