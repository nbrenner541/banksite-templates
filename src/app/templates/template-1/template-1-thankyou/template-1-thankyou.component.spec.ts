import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template1ThankyouComponent } from './template-1-thankyou.component';

describe('Template1ThankyouComponent', () => {
  let component: Template1ThankyouComponent;
  let fixture: ComponentFixture<Template1ThankyouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template1ThankyouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template1ThankyouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
