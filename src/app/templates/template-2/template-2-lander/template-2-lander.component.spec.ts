import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template2LanderComponent } from './template-2-lander.component';

describe('Template2LanderComponent', () => {
  let component: Template2LanderComponent;
  let fixture: ComponentFixture<Template2LanderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template2LanderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template2LanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
