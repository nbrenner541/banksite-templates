import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template2ThankyouComponent } from './template-2-thankyou.component';

describe('Template2ThankyouComponent', () => {
  let component: Template2ThankyouComponent;
  let fixture: ComponentFixture<Template2ThankyouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template2ThankyouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template2ThankyouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
