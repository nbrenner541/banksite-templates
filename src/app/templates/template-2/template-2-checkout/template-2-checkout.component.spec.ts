import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Template2CheckoutComponent } from './template-2-checkout.component';

describe('Template2CheckoutComponent', () => {
  let component: Template2CheckoutComponent;
  let fixture: ComponentFixture<Template2CheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Template2CheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Template2CheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
